a.out: main.cpp
	g++ main.cpp -fopenmp -std=c++11

secuencial: a.out
	perl run.pl 1 100

paralelo: a.out
	perl run.pl

clean:
	rm -f resultado* a.out *.csv

all:
	secuencial
