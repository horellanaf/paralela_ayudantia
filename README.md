# Tarea 1 ayudantia Paralela

## Integrantes

Nicolas Toro
Marcos Gomez
Hector Orellana

## Compilar el programa

```
g++ main.cpp --std=c++11 -fopenmp
```

## Ocupar el programa

El angulo y los archivos mesh y node se pasan al programa como argumentos.

```
$ ./a.out 120 archivo.mesh archivo.node

Se refinaron 3 triangulos
```

Luego de ejecutado el programa en la misma carpeta existiran 2 archivos nuevos: resultado.node y resultado.mesh

## Medir el tiempo de ejecucion

Hay un script, run.pl que permite medir el tiempo de ejecucion del programa. Recibe como argumentos el numero de procesadores a utilizar y la cantidad de veces que se desea ejecutar el programa.

Luego de ejecutar el script run.pl deberia existir un archivo datos.csv el cual contiene la informacion generada. A partir de este archivo se hizo el grafico t vs 2^n.

```
perl run.pl
```

## Make

Tambien hay un Makefile que compila y ejecuta el programa.

Permite ejecutar el programa secuencialmente:

```
make secuencial
```

o en paralelo

```
make paralelo
```
