#include <iostream>
#include <string>
#include <cstring>
#include <algorithm>
#include <fstream>
#include <vector>
#include <iomanip>
#include <cmath>

#include <omp.h>

#define PI 3.14159265

class Coordenada {
public:
  double x;
  double y;

public:
  Coordenada(double x, double y) {
    this->x = x;
    this->y = y;
  }

  Coordenada(std::vector<double> par) {
    this->x = par[0];
    this->y = par[1];
  }

  bool operator==(Coordenada b) {
    return
      this->x == b.x &&
      this->y == b.y;
  }

  std::string to_string() {
    char buffer[2048];
    snprintf(buffer, 2048, "<Coordenada x=%f y=%f>", this->x, this->y);
    return buffer;
  }
};

class Triangulo {
  //     B
  //    / \
  //   /   \
  //  /     \
  // /_______\
  // A       C

private:
  std::vector<Coordenada> puntos() {
    std::vector<Coordenada> r;

    r.push_back(this->a);
    r.push_back(this->b);
    r.push_back(this->c);

    return r;
  }

  double distancia(Coordenada a, Coordenada b) {
    double dx = a.x - b.x;
    double dy = a.y - b.y;

    return std::sqrt((dx * dx) + (dy * dy));
  }

  std::vector<Coordenada> arista_mas_larga() {
    std::vector<Coordenada> r;
    // Retorna un vector con los 3 puntos
    // Los 2 primeros elementos son los puntos de la arista mas larga
    // el 3 es el punto opuesto a la arista mas larga.
    if (this->ab() > this->ac() && this->ab() > this->bc()) {
      r.push_back(a);
      r.push_back(b);
      r.push_back(c);
      // return std::vector<Coordenada>(this->a, this->b, this->c);
    }
    else if (this->ac() > this->ab() && this->ac() > this->bc()) {
      r.push_back(a);
      r.push_back(c);
      r.push_back(b);

      // return std::vector<Coordenada>(this->a, this->c, this->b);
    }
    else {
      r.push_back(b);
      r.push_back(c);
      r.push_back(a);
      // return std::vector<Coordenada>(this->b, this->c, this->a);
    }

    return r;
  }

  Coordenada mitad(Coordenada a, Coordenada b) {
    double x = (a.x + b.x) / 2;
    double y = (a.y + b.y) / 2;
    return Coordenada(x, y);
  }

public:
  Coordenada a;
  Coordenada b;
  Coordenada c;

  Triangulo() : a(Coordenada(0, 0)),
                b(Coordenada(0, 0)),
                c(Coordenada(0, 0)) {};

  Triangulo(Coordenada a, Coordenada b, Coordenada c) : a(a), b(b), c(c) {};

  Triangulo& operator= (const Triangulo &other) {
    this->a = other.a;
    this->b = other.b;
    this->c = other.c;

    return *this;
  }

  double ab() {
    return this->distancia(a, b);
  }

  double ac() {
    return this->distancia(a, c);
  }

  double bc() {
    return this->distancia(b, c);
  }

  double angulo_A() {
    double numerador =
      std::pow(this->ab(), 2) +
      std::pow(this->ac(), 2) -
      std::pow(this->bc(), 2);

    double denominador = 2.0 * this->ab() * this->ac();

    return std::acos(numerador / denominador) * 180 / PI;
  }

  double angulo_B() {
    double numerador =
      std::pow(this->ab(), 2) +
      std::pow(this->bc(), 2) -
      std::pow(this->ac(), 2);

    double denominador = 2.0 * this->ab() * this->bc();

    return std::acos(numerador / denominador) * 180 / PI;
  }

  double angulo_C() {
    double numerador =
      (this->bc() * this->bc()) +
      (this->ac() * this->ac()) -
      (this->ab() * this->ab());

    double denominador = 2 * this->bc() * this->ac();

    return std::acos(numerador / denominador) * 180 / PI;
  }


  bool contiene_angulo(double angulo) {
    return
      angulo <= this->angulo_A() ||
      angulo <= this->angulo_B() ||
      angulo <= this->angulo_C();
  }

  std::vector<Triangulo> biseccionar() {
    std::vector<Triangulo> nuevos_triangulos;

    auto c = this->arista_mas_larga();

    Coordenada d = mitad(c[0], c[1]); // Punto medio arista mas larga
    Coordenada a = c[2]; // punto opuesto a arista mas alrga

    for (auto p : this->puntos()) {
      if (p == a)
        continue;

      Triangulo nuevo(a, d, p);
      nuevos_triangulos.push_back(nuevo);
    }

    return nuevos_triangulos;
  }

  std::string to_string() {
    char buffer[2048];
    snprintf(buffer, 2048, "<Triangulo a=%s b=%s c=%s>",
             this->a.to_string().c_str(), this->b.to_string().c_str(), this->c.to_string().c_str());
    return std::string(buffer);
  }
};


// https://stackoverflow.com/a/14266139
std::vector<std::string> split(std::string s, std::string delimiter) {
  std::vector<std::string> result;

  int pos = s.find(delimiter);

  while ((pos = s.find(delimiter)) != std::string::npos) {
    auto token = s.substr(0, pos);
    result.push_back(token);
    s.erase(0, pos + delimiter.length());
  }

  result.push_back(s);

  return result;
}

std::vector<std::vector<int> > cargar_mesh(char *archivo_mesh) {
  std::vector<std::vector<int> > mesh;

  std::ifstream f_mesh(archivo_mesh);
  std::string linea;

  while (std::getline(f_mesh, linea)) {
    auto palabras = split(linea, " ");

    if (palabras.size() < 3) {
      continue;
    }

    std::vector<int> puntos;

    for (auto palabra : palabras) {
      auto i_palabra = std::atoi(palabra.c_str());
      puntos.push_back(i_palabra);
    }

    mesh.push_back(puntos);
  }

  return mesh;
}

std::vector<std::vector<double> > cargar_node(char *archivo_node) {
  std::vector<std::vector<double> > node;

  std::ifstream f_node(archivo_node);
  std::string linea;

  while (std::getline(f_node, linea)) {
    std::vector<std::string> palabras = split(linea, " ");

    if (palabras.size() < 3) {
      continue;
    }

    std::vector<double> coordenadas;


    coordenadas.push_back(std::atof(palabras[1].c_str()));
    coordenadas.push_back(std::atof(palabras[2].c_str()));

    node.push_back(coordenadas);
  }

  return node;
}

std::vector<Triangulo> cargar_triangulos(char *archivo_mesh, char *archivo_node) {
  auto mesh = cargar_mesh(archivo_mesh);
  auto node = cargar_node(archivo_node);

  std::vector<Triangulo> triangulos(mesh.size());

  // #pragma omp parallel for
  for (int i = 0; i < mesh.size(); i++) {
    auto m = mesh[i];

    Coordenada a(node[m[0] - 1]);
    Coordenada b(node[m[1] - 1]);
    Coordenada c(node[m[2] - 1]);

    Triangulo t(a, b, c);
    triangulos[i] = t;
  }

  return triangulos;
}


bool validar_angulo(char *angulo) {
  bool encontre_punto = false;

  for (int i = 0; i < strlen(angulo); i++) {
    if (angulo[i] == '-' && i != 0)
      return false;

    if (angulo[i] == '-' && i == 0)
      continue;

    if (angulo[i] == '.' && !encontre_punto) {
      encontre_punto = true;
      continue;
    }

    if (angulo[i] == '.' && !encontre_punto)
      return false;

    if (!std::isdigit(angulo[i]))
      return false;
  }

  return true;
}


int main(int argc, char **argv) {
  std::cout << omp_get_num_threads() << std::endl;
  std::cout << omp_get_num_procs() << std::endl;

  if (argc < 4) {
    std::cerr << "./a.out angulo archivo.mesh archivo.node" << std::endl;
    return 1;
  }

  if (!validar_angulo(argv[1])) {
    std::cerr << "Angulo: " << argv[1] << " no es valido" << std::endl;
    return -1;
  }


  double angulo = std::atof(argv[1]);

  std::vector<Triangulo> triangulos = cargar_triangulos(argv[2], argv[3]);

  std::vector<Triangulo> resultado((triangulos.size() * 2) + triangulos.size());

  int triangulos_refinados = 0;

  for (int i = 0; i < triangulos.size(); i++) {
    auto t = triangulos[i];

      if (t.contiene_angulo(angulo)) {
        triangulos_refinados = triangulos_refinados + 1;

        auto nuevos_triangulos = t.biseccionar();

        #pragma omp parallel for
        for (int j = 0; j < nuevos_triangulos.size(); j++)
          resultado[resultado.size()] = nuevos_triangulos[j];
      }
  }

  std::ofstream mesh_nuevo("resultado.mesh");
  std::ofstream node_nuevo("resultado.node");

  std::cout << "Se refinaron " << triangulos_refinados / omp_get_num_threads() << " triangulos" << std::endl;

  int linea = 1;
  for (auto t : resultado) {
    node_nuevo << linea << " " << t.a.x << " " << t.a.y << std::endl;
    mesh_nuevo << linea << " ";
    linea++;

    node_nuevo << linea << " " << t.b.x << " " << t.b.y << std::endl;
    mesh_nuevo << linea << " ";
    linea++;

    node_nuevo << linea << " " << t.c.x << " " << t.c.y << std::endl;
    mesh_nuevo << linea << " ";
    linea++;

    mesh_nuevo << std::endl;
  }

  return 0;
}
