#!perl

use strict;
use warnings;

use Time::HiRes qw(time);

sub measure_once {
  my ($cmd) = @_;

  my $start = time;
  `$cmd`;
  my $end = time;

  return $end - $start;
}

sub measure {
  my ($cmd, $times) = @_;


  my @measurements;

  for (my $i = 0; $i < $times; $i++) {
    my $measure = measure_once($cmd);
    push @measurements, $measure;
  }

  return ((eval join "+", @measurements) / $times);
}

sub restrict {
  my ($cmd, $cpus) = @_;
  my $cpus_string = join(',', enumerate_cpus($cpus));

  $cmd = "taskset --cpu-list $cpus_string $cmd";

  return $cmd;
}

sub enumerate_cpus {
  my ($cpu_count) = @_;
  my @cpus = ();

  if ($cpu_count < 1) {
    return (0);
  }

  for (my $i = 0; $i < $cpu_count; $i++) {
    push @cpus, $i;
  }

  return @cpus;
}

my ($cpu_count, $n, $angulo) = @ARGV;

if (!$cpu_count) {
  print "Se usaran desde 2^0 a 2^x procesadores. Ingrese x: ";
  $cpu_count = int(<STDIN>);
}

if (!$n) {
  print "Ingrese cantidad de veces que desea que el programa se ejecute: ";
  $n = int(<STDIN>);
}

if (!$angulo) {
  print "Ingrese el angulo: ";
  $angulo = int(<STDIN>);
}

open(my $fh, ">", "datos.csv");

print $fh "Numero Procesadores;Tiempo medido (Promedio);Cantidad Intentos;Comando Ejecutado\n";

for (my $i = 0; $i <= $cpu_count; $i = $i + 1) {
  my $cmd = restrict("./a.out $angulo archivo.mesh archivo.node", 2 ** $i);
  my $measurement = measure($cmd, $n);

  print $fh "@{[2 ** $i]};$measurement;$n;$cmd\n";

  if ($cpu_count == 1) {
    last;
  }
}
